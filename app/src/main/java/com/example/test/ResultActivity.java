package com.example.test;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class ResultActivity extends AppCompatActivity {

    //variables de entorno
    public float [ ] datax = new float[100] ;
    public float [ ] datay = new float[100] ;
    public int n;
    SLR linear;
    TextView display;
    TextView display2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        setContentView(R.layout.activity_result);

        //get date of the past activity
        datax = intent.getFloatArrayExtra("x_values");
        datay = intent.getFloatArrayExtra("y_values");
        n = intent.getIntExtra("n_value",100 );

        display = findViewById(R.id.displayr);
        display2 = findViewById(R.id.formula);
        linear = new SLR(datax,datay,n);
    }


    @SuppressLint("SetTextI18n")
    public void botonclick(View v){

        EditText text01 = findViewById(R.id.predict);
        try{
            float numero = Float.parseFloat(text01.getText().toString());
            float resultado = linear.prediction(numero);
            String formula = linear.formu();

            display.setText("Prediccion de Y: "+Float.toString(resultado));
            display2.setText(formula);
        }catch ( NumberFormatException e){}


    }




}
