package com.example.test;

public class SLR {
    public float [ ] datax = new float[100] ;
    public float [ ] datay = new float[100] ;
    float sx = 0, sy = 0, sx2 = 0, sxy = 0;
    int n;
    float  b1, b0;

    //contructores
    public SLR(float[] datax, float[] datay, int n) {
        this.datax = datax;
        this.datay = datay;
        this.n = n;
    }

    public SLR(float[] datax, float[] datay) {
        this.datax = datax;
        this.datay = datay;
        this.n = datax.length;
    }

    //procesos
    public float prediction(float x){
        init();
        b0 = ((n*sxy) -(sx * sy )) / ((n *sx2) -(sx * sx));
        b1 = ((sy - (b0 * sx)) / n );
        return (b1 + (b0 * x));
    }

    public String formu(){
        String formula;
        init();
        b0 = ((n*sxy) -(sx * sy )) / ((n *sx2) -(sx * sx));
        b1 = ((sy - (b0 * sx)) / n );
        formula = " Y = ("+Float.toString(b1)+") + ("+Float.toString(b0)+")X";
        return formula;
    }

    private void sumas(){
        for(int i = 0; i < this.n; i++) {
            sx += datax[i];
            sy += datay[i];
            sx2 += datax[i] * datax[i];
            sxy += datax[i] * datay[i];
        }
    }

    private void empty(){
        sx = 0;
        sy = 0;
        sx2 =0;
        sxy =0;
    }
    private void init(){
        empty();
        sumas();
    }



    //getters and setters
    public float[] getDatax() {
        return datax;
    }

    public void setDatax(float[] datax) {
        this.datax = datax;
    }

    public float[] getDatay() {
        return datay;
    }

    public void setDatay(float[] datay) {
        this.datay = datay;
    }
}
