package com.example.test;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import com.androidplot.xy.*;

import java.util.Arrays;

public class PlotActivity extends AppCompatActivity {

    public float [ ] datax = new float[100] ;
    public float [ ] datay = new float[100] ;
    public float [ ] predicty = new float[100];
    SLR linear;
    public int n;
    private XYPlot plot;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_plot);
        Intent intent = getIntent();

        datax = intent.getFloatArrayExtra("x_values");
        datay = intent.getFloatArrayExtra("y_values");
        n = intent.getIntExtra("n_value",100 );
        linear = new SLR(datax,datay,n);

        for( int i=0;i<n;i++){
            predicty[i] = linear.prediction(datax[i]);
        }
        // initialize our XYPlot reference:
        plot = findViewById(R.id.plot);

        XYSeries series1 = generateScatter("Dataset", n, datax ,datay,new RectRegion(-100, 100, -100, 100));
        XYSeries series2 = generateScatter("slr", n, datax ,predicty,new RectRegion(-100, 100, -100, 100));
        // create formatters to use for drawing a series using LineAndPointRenderer
        // and configure them from xml:
        LineAndPointFormatter series1Format = new LineAndPointFormatter(this, R.xml.point_formatter);
        LineAndPointFormatter series2Format = new LineAndPointFormatter(this, R.xml.line_point_formatter_with_labels);


        // add each series to the xyplot:
        plot.addSeries(series1, series1Format);
        plot.addSeries(series2, series2Format);

        // reduce the number of range labels
        plot.setLinesPerRangeLabel(3);


    }

    private XYSeries generateScatter(String title, int numPoints,float dx [],float dy [], RectRegion region) {
        SimpleXYSeries series = new SimpleXYSeries(title);
        for(int i = 0; i < numPoints; i++) {
            series.addLast(dx[i], dy[i]);
        }
        return series;
    }

}
